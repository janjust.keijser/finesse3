.. include:: ../defs.hrst

.. _debugging:

Debugging Finesse
=================

Errors originating from C code compiled via Cython often do not yield useful error
messages. Instead you may encounter segmentation faults without any additional
information to point to the error origin. There are a few tools that can help you figure
out the error, listed here.

Compile Cython extensions with debug symbols
--------------------------------------------

Code is normally built without debug symbols. This provides a speed benefit but makes it
harder to trace error locations. To build |Finesse| with debug symbols enabled, run
``make debug`` instead of just ``make`` in the root |Finesse| directory.

Run the code using a debugger
-----------------------------

Unix platforms usually have ``lldb`` and ``gdb`` available as a system package.

Example debug incantation using ``gdb``::

    $ gdb --ex r --args python /path/tp/script.py
