.. include:: ../../../defs.hrst

Checking a Cython extension for CPython interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When installing |Finesse|, during the build process of the Cython extensions, HTML files
are generated for all `.pyx` files which show the level of interaction each line has
with the Python Virtual Machine.

An example of this is shown below. The yellow lines indicate Python interaction with the
opaqueness of the colour giving the level of this interaction - i.e. stronger yellow
means more Python interaction. These lines can be expanded to show exactly which CPython
functions are being called.

When inspecting these annotated HTML files you should look out for any yellow lines
present in the bodies of loops. It is these lines which are likely to be the culprits of
bottlenecks in the Cython extension performance.

.. figure:: images/camera_annotated_html.png
    :align: center
