.. include:: ../../../defs.hrst

.. _cython_documenting:

Documenting Cython extensions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Writing documentation for the Cython parts of |Finesse| works largely the same as for the
Python parts, but with a few subtleties arising due to the implicit use of types. One of
the key things to note is that any `cdef` functions (regardless of whether they exist as
part of a `cdef` class or not) with docstrings will not be documented during the Sphinx
build process - this should be fairly obvious given that `cdef` functions are not
exposed to the Python API. This is not a problem for the most part, as only |Finesse|
developers involved with writing Cython extensions will use `cdef` functions; everything
accessible by |Finesse| users (i.e. everything *but* `cdef` functions) is documentable.
