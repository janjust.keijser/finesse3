.. include:: /defs.hrst

.. _cav_eigenmode:

Cavity eigenmodes
*****************

A cavity eigenmode is defined as the optical field whose spatial properties are such
that the field after one round-trip through the cavity will be exactly the same as the
injected field :cite:`LivingReview`. In the case of resonators with spherical mirrors,
the eigenmode will be a Gaussian mode, defined by the Gaussian beam parameter qcav. For
a generic cavity (an arbitrary number of spherical mirrors or lenses) a round-trip ABCD
matrix :math:`M_{\mathrm{rt}}` can be defined and used to compute the cavity’s
eigenmode.

The change in the :math:`q` parameter after one round-trip through a cavity is given by:

.. math::
    \frac{A q_{1} + B}{C q_{1} + D} = q_{2} = q_{1}

where :math:`A`, :math:`B`, :math:`C` and :math:`D` are the elements of a matrix
:math:`M_{\mathrm{rt}}`. If :math:`q_1 = q_2` then the spatial profile of the beam is
recreated after each round-trip and we have identified the cavity eigenmode. We can
compute the parameter :math:`q_{\mathrm{cav}} \equiv q_1 = q_2` by solving:

.. math::
    C q_{\mathrm{cav}}^2+(D-A)q_{\mathrm{cav}} - B = 0,

An example of this is shown in :numref:`fig_round_trip_abcd` where the round trip matrix
is given at the top of the figure. From this, we can compute the :math:`A`, :math:`B`,
:math:`C` and :math:`D` coefficients for the round-trip matrix to solve the eigenmode
equation above. This quadratic equation generally has two solutions, one being the
complex conjugate of the other.

.. _fig_round_trip_abcd:
.. figure:: images/spatial_eigenmodes_cavity.*
    :align: center

    Cavity round trip ABCD matrix for a Fabry-Perot cavity, with corresponding wavefront
    curvature.

When the polynomial above has a suitable solution the optical resonator is said to be
"stable". The stability requirement can be formulated using the Gaussian beam parameter:
a cavity is stable only when the cavity's eigenmode, :math:`q_{\mathrm{cav}}`, has a
real waist size. The value for the beam waist is a real number whenever
:math:`q_{\mathrm{cav}}` has a positive non-zero imaginary part, as this defines the
Rayleigh range of the beam and therefore the beam waist, :math:`\Im{(q_{\mathrm{cav}})}
= \pi w_0^2/\lambda`. A complex :math:`q_{\mathrm{cav}}` is ensured if the determinant
of the cavity eigenmode equation is negative.

This requirement can formulated in a compact way by defining the parameter :math:`m` as:

.. math::
    m \equiv \frac{A+D}{2},

where :math:`A` and :math:`B` are the coefficients of the round-trip matrix
:math:`M_{\mathrm{rt}}`. The stability criterion then simply becomes:

.. math:
    m^2 < 1.

The stability of simple cavities are often described using *g-factors*. These factors
are simply rescaled versions of the more generic :math:`m` value:

.. math::
    g \equiv \frac{m+1}{2} = \frac{A+D+2}{4},

.. note::

    |Finesse| uses g-factors for stability attributes in the
    :class:`finesse.components.cavity.Cavity` class. Individual g-factors for both the
    tangential and sagittal planes are provided by this class.

For the cavity to be stable the g-factor must fulfil the requirement:

.. math::
    0 \leq g \leq 1

The closer :math:`g` is to 0 or 1, the smaller the tolerances are for any change in the
geometry before the cavity becomes unstable.

For a simple two-mirror cavity, such as the one given in :numref:`fig_round_trip_abcd`,
its g-factor is

.. math::
    \begin{array}{l}
        g_1 = 1 - \frac{L}{R_{c,1}}, \\
        g_2 = 1 - \frac{L}{R_{c,2}}, \\
        g = g_1 g_2.
    \end{array}

Where :math:`g_{1,2}` are the individual g-factors of the cavity mirrors and :math:`g`
is the g-factor of the entire cavity.
