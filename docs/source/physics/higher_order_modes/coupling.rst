.. include:: /defs.hrst

.. _hom_couplings:

Coupling of higher-order-modes
******************************

*Note that much of this content is based on this review* :cite:`LivingReview`

Solving the overlap integral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The coupling of a mode refers to how a spatial mode in one basis is represented in
another; e.g. which sum of modes in the cavity basis :math:`q_2` produces the
:math:`\mathrm{HG}_{00}` mode in the :math:`q_1` basis. Hermite–Gauss modes are coupled
whenever a beam is not matched or aligned to a cavity or beam segment. This coupling is
sometimes referred to as scattering into higher-order modes because in most cases the
laser beam is a considered as a pure :math:`\mathrm{HG}_{00}` mode and any mode coupling
would transfer power from the fundamental into higher-order modes. However, in general
every mode with non-zero power will transfer energy into other modes whenever mismatch
or misalignment occur, and this effect also includes the transfer from higher orders
into a low order.

To compute the amount of coupling the beam must be projected into the base system of the
cavity or beam segment it is being injected into. This is always possible, provided that
the paraxial approximation holds, because each set of Hermite–Gauss modes, defined by
the beam parameter at a position :math:`z`, forms a complete set. Such a change of the
basis system results in a different distribution of light power in the new Hermite–Gauss
modes and can be expressed by coupling coefficients that yield the change in the light
amplitude and phase with respect to mode number.

Let us assume that a beam described by the beam parameter :math:`q_1` is injected into a
segment described by the parameter :math:`q_2`. Let the optical axis of the beam be
misaligned: the coordinate system of the beam is given by (:math:`x, y, z`) and the beam
travels along the :math:`z`-axis. The beam segment is parallel to the :math:`z'`-axis
and the coordinate system (:math:`x', y', z'`) is given by rotating the (:math:`x, y,
z`) system around the :math:`y`-axis by the *misalignment angle* :math:`\gamma`. The
amplitude of a particular mode :math:`\mathrm{TEM}_{nm}` in the beam segment is then
defined as:

.. math::
    u_{n m}(x,y;\,q_2)\exp{\left(i(\omega t -k z)\right)}=\sum_{n',m'}k_{n,m,n',m'}u_{n'
    m'}(x,y;\,q_1)\exp{\left(i(\omega t -k z')\right)},

where :math:`u_{n' m'}(x,y;\,q_1)` are the HG modes used to describe the injected beam,
:math:`u_{n m}(x,y;\,q_2)` are the "new" modes that are used to describe the light in
the beam segment and :math:`k_{n,m,n',m'}` is the *coupling coefficient* from each
:math:`\mathrm{TEM}_{n'm'}` into :math:`\mathrm{TEM}_{nm}`.

Note that including the plane wave phase propagation within the definition of coupling
coefficients is important because it results in coupling coefficients that are
independent of the position on the optical axis for which the coupling coefficients are
computed.

Using the fact that the HG modes :math:`u_{n m}` are orthonormal, we can compute the
coupling coefficients by the overlap integral :cite:`BayerHelms`:

.. math::
    k_{n,m,n',m'}=\exp{\left(i 2 k z'
    \sin^2\left(\frac{\gamma}{2}\right)\right)}\int\!\!\!\int\!dx'dy'~
    u_{n' m'}\exp{\left(i k x' \sin{\gamma}\right)}~u^*_{n m}.


Since the Hermite--Gauss modes can be separated with respect to :math:`x` and :math:`y`,
the coupling coefficients can also be split into :math:`k_{n m n' m'}=k_{n n'}k_{m m'}`.
These equations are very useful in the paraxial approximation as the coupling
coefficients decrease with large mode numbers. In order to be described as paraxial, the
angle :math:`\gamma` must not be larger than the diffraction angle. In addition, to
obtain correct results with a finite number of modes the beam parameters :math:`q_1` and
:math:`q_2` must not differ too much.

The integral can be computed directly using numerical integration methods. However, this
can potentially be computationally very expensive depending on how difficult the
integrand is to evaluate and complex it is. The following part of this section is based
on the work of Bayer-Helms :cite:`BayerHelms` and provides an analytic solution to the
integral. In :cite:`BayerHelms` the above integral is partly solved and the coupling
coefficients are given by multiple sums as functions of :math:`\gamma` and the mode
mismatch parameter :math:`K`, which is defined by

.. math::
    K=\frac{1}{2} (K_0+iK_2),

where :math:`K_0=(z_R-z_R')/z_R'` and :math:`K_2=((z-z_0)-(z'-z_0'))/z_R'`. This
can also be written using :math:`q=i\zr +z-z_0`, as

.. math::
    K=\frac{i (q-q')^*}{2 \Im{q'}}.

The coupling coefficients for misalignment and mismatch (but no lateral displacement)
can then be written as

.. math::
    k_{n n'}=(-1)^{n'} E^{(x)} (n!n'!)^{1/2} (1+K_0)^{n/2+1/4}
    (1+K^*)^{-(n+n'+1)/2}\left\{S_g-S_u\right\},

where

.. math::
    \begin{array}{l}
    S_g=\sum\limits_{\mu=0}^{[n/2]}\sum\limits_{\mu'=0}^{[n'/2]}
    \frac{(-1)^\mu \bar{X}^{n-2\mu}X^{n'-2\mu'}}{(n-2\mu)!(n'-2\mu')!}
    \sum\limits_{\sigma=0}^{\min(\mu,\mu')}\frac{(-1)^\sigma
    \bar{F}^{\mu-\sigma} F^{\mu'-\sigma}}
    {(2\sigma)! (\mu-\sigma)! (\mu'-\sigma)!},\\
    S_u=\sum\limits_{\mu=0}^{[(n-1)/2]}\sum\limits_{\mu'=0}^{[(n'-1)/2]}
    \frac{(-1)^\mu \bar{X}^{n-2\mu-1}X^{n'-2\mu'-1}}{(n-2\mu-1)!(n'-2\mu'-1)!}
    \sum\limits_{\sigma=0}^{\min(\mu,\mu')}\frac{(-1)^\sigma
    \bar{F}^{\mu-\sigma} F^{\mu'-\sigma}}
    {(2\sigma+1)! (\mu-\sigma)! (\mu'-\sigma)!}.
    \end{array}

The corresponding formula for :math:`k_{m m'}` can be obtained by replacing
the following parameters: :math:`n\rightarrow m`, :math:`n'\rightarrow m'`,
:math:`X,\bar{X}\rightarrow 0` and :math:`E^{(x)}\rightarrow 1` (see below). The
notation :math:`[n/2]` means

.. math::
    \left[\frac{m}{2}\right]=\left\{
    \begin{array}{ll}
    m/2 & \text{if}\ m\ \text{is even,}\\
    (m-1)/2 & \text{if}\ m\ \text{is odd.}
    \end{array}\right.

The other abbreviations used in the above definition are

.. math::
    \begin{array}{l}
    \bar{X}={(i \zr'-z')\sin{(\gamma)}}/({\sqrt{1+K^*}w_0}),\\
    X={(i \zr+z')\sin{(\gamma)}}/({\sqrt{1+K^*}w_0}),\\
    F={K}/({2(1+K_0)}),\\
    \bar{F}={K^*}/{2},\\
    E^{(x)}=\exp{\left(-\frac{X\bar{X}}{2}\right)}.
    \end{array}

.. _scatter_matrices:

Scattering matrices
~~~~~~~~~~~~~~~~~~~

|Finesse| computes and stores *scattering matrices* to describe how spatial modes couple
with respect to each other between beam parameter bases. A scattering matrix is simply a
square matrix of coupling coefficients where each coefficient represents how one mode
couples into another - i.e. the scaling between the modes in terms of both the amplitude
and phase of the field.

:numref:`fig_m_couplings` shows the four scattering matrices present at a mirror M. For
a mode-matched and aligned system, each of these matrices are just identity matrices.

.. _fig_m_couplings:
.. figure:: images/mirror_couplings.*
    :align: center

    Scattering matrices for a mirror. K11, K22 are the reflection coupling matrices
    whilst K12 and K21 are the transmission coupling matrices.

To compute the overall field couplings at the mirror M, |Finesse| multiplies the
corresponding element of the mirrors' local coupling matrix by the associated scattering
matrix. For example, to compute the coupling of the reflected field from the first
surface, :math:`m_{11}`, the equation,

.. math::
    m_{11} = r K_{11} \exp{\left(i\varphi\right)},

is applied, where :math:`r = \sqrt{R}` with :math:`R` as the refectivity of the mirror
and :math:`\varphi` is the phase.

API for coupling coefficient calculations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Functions for computing and modifying scattering matrices can be found in this
submodule:

.. autosummary::

    finesse.knm
