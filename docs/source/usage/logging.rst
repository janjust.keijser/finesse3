.. include:: /defs.hrst
.. _logging:

Viewing logs
============

|Finesse| contains a comprehensive logging system, issuing useful information during the
parsing, building, running and output phases of a kat or Python script.

Making log messages visible
---------------------------

Depending on how you run |Finesse|, log messages with `WARNING` level or higher may
already by default get printed to the standard output. This is the case when running
|Finesse| interactively such as via a Jupyter notebook or IPython shell, or via the
:ref:`command line interface <cli>`. If you run |Finesse| via a normal Python shell or
via the ``python`` command, however, logging will not be enabled by default in order to
give scripts maximum flexibility over how log messages are handled.

In any case, you may still wish to control the *level* of the messages made visible by
the logging system, such as to show `INFO` and/or `DEBUG` messages in addition to
`WARNING` and `ERROR` messages. If you use |Finesse| via a script or notebook or
terminal, you have three options:

Option 1: use command line interface option
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The command line interface (CLI) supports changing the log verbosity via option flags.
See the :ref:`CLI docs <cli>` for more information.

Option 2: call 'finesse.config.configure'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The function :func:`finesse.config.configure` can be imported from :mod:`finesse` and
used to set the printed log level::

    from finesse import configure

    configure(log_level="info")  # ...or one of "debug", "warning", "error", "critical".

    # Info or higher logs emitted by code from now on will be shown.

Logs with level at or above that which you specify will from this point onwards be
displayed.

Option 3: wrap your code in a 'finesse.utilities.misc.logs' context
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The context manager :func:`~finesse.utilities.misc.logs` provides a quick way to turn on
verbose logging for a small block of code::

    from finesse import Model
    from finesse.utilities import logs

    model = Model()

    with logs(level="info"):
        # Info messages emitted by the following call get printed to stderr.
        model.parse("laser l1 P=1")

Option 4: configure via the Python 'logging' module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This approach with the :py:mod:`logging` module is useful if you use |Finesse| as part 
of a collection of other Python programs or libraries, where you wish to configure 
logging for all of these tools together in one place::

    import logging

    # Configure logging for all subsequent libraries.
    logging.basicConfig(level=logging.INFO)

    # Any INFO or higher logs emitted during the import of the following libraries will
    # be printed to stderr.
    import matplotlib
    import finesse

Configuring logging via this method is the most powerful, and allows you to send log
messages to files or over the web and to apply custom formatting to messages. For more
information, see for instance this `Python logging guide
<https://docs.python.org/3/howto/logging.html>`__.

Filtering log channels
----------------------

You may wish to exclude a particular channel from being displayed by the logging system.
You can do this by passing a list of channels to be excluded to the `log_exclude`
parameter of :func:`~finesse.config.configure`. Simple wildcards are supported: use
``*`` for zero or more characters, ``?`` to match zero or one of the previous character,
``[seq]`` to match any character in `seq`, and ``[!seq]`` to match any character *not*
in `seq`. For example, to exclude all `script` logs you could write
``configure(log_exclude=["*script*])``.
