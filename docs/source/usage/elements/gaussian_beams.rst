.. include:: /defs.hrst

==============
Gaussian beams
==============

.. kat:element:: cavity

.. kat:element:: gauss
