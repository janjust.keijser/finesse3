.. include:: ../defs.hrst
.. _installation:

Installation
============

The recommended way to install |Finesse| is to use the Conda package. This handles the
installation of the system and Python requirements in order to run |Finesse|, and is
available on most platforms.

Alternatively, depending on your system, there may be a pre-built package available to
install from `PyPI <https://pypi.org/project/finesse/>`__ using your favorite Python
package manager.

Via Conda
---------

.. todo:: Document installing Finesse Conda package once available

Via PyPI
--------

With this approach, it is recommended to install |Finesse| in a virtual environment.
This ensures that dependencies required for |Finesse| to run do not conflict with other
packages installed on your computer.

.. seealso::

    For more information on virtual environments, see the `Virtual Environments and
    Packages <https://docs.python.org/3/tutorial/venv.html>`__ section of the Python
    documentation.

|Finesse| is available on PyPI with the package name ``finesse``. There are many package
managers available for Python but the most common is `pip
<https://pip.pypa.io/en/stable/installing/>`__. Instructions for setting up a virtual
environment and installing |Finesse| via pip are given for each supported platform
below.

Linux / Mac OSX
~~~~~~~~~~~~~~~

To create a virtual environment, open up a terminal and run:

.. code-block:: console

    $ python -m venv finesse

Activate the virtual environment in your current terminal using:

.. code-block:: console

    $ source finesse/bin/activate

Install |Finesse| via pip with:

.. code-block:: console

    (finesse) $ pip install finesse

Windows
~~~~~~~

To create a virtual environment, open up a DOS command prompt and run:

.. code:: doscon

    C:\> py -m venv finesse

Activate the virtual environment in your current command prompt using:

.. code-block:: doscon

    C:\> finesse\Scripts\activate.bat

Install |Finesse| via pip with:

.. code-block:: doscon

    (finesse) C:\> pip install finesse
