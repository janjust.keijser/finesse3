import numpy as np
import finesse

finesse.LOGGER.setLevel("DEBUG")

model = finesse.Model()
model.parse_legacy(
    """
# laser and EOM
l i1 1 0 n00
s s0_0 0 n00 n0
mod eo1 40k 0.3 3 pm n0 n10
s s0_1 0 n10 n1
m m1 0.9 0.1 0 n1 n2
s s1 1200 n2 n3
m m2 .9 0.01 0 n3 n4

pd1 pdh 40k 0 n1
pd circ n2
xaxis m2 phi lin 0 100 200
"""
)

model.add(
    finesse.locks.Lock(
        "z", model.pdh, model.m1.phi, -10, 1e-8, initial_only=False, additive_fb=True
    )
)

out = model.run()

print(np.allclose(out["z"], out.x1))
out.plot()


import pykat

base = pykat.finesse.kat(
    kat_code="""
# laser and EOM
l i1 1 0 n00
s s0_0 0 n00 n0
mod eo1 40k 0.3 3 pm n0 n10
s s0_1 0 n10 n1
m m1 0.9 0.1 0 n1 n2
s s1 1200 n2 n3
m m2 .9 0.01 0 n3 n4

pd1 pdh 40k 0 n1
pd circ n2
xaxis m2 phi lin 0 100 400

# set the error signal to be photodiode output (`re' stands
# for the real part of the diode output. `re' has to be used
# with diodes that provide a real output.
set err pdh re
# Perform the lock! Gain is -10 and the accuracy 10n ( = 1e-8)
lock z $err -10 10n
# ... and connect the feedback to the interferometer
put* m1 phi $z
"""
)
out2 = base.run()
out2.plot()
