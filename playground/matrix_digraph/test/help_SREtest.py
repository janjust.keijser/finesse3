# -*- coding: utf-8 -*-
# Code originally from phasor2, Lee McCuller (Apache license v2.0)
# modified to include some additional documentation
"""
"""
import numpy as np
import collections
import scipy.sparse as scisparse

from finesse.simulations.matrix_digraph.utilities import SRE_copy


def sparse_gen(
    N, node_avg_size, node_names, node_sizes_d, node_offsets, node_offsets_d
):
    seq = collections.defaultdict(set)
    req = collections.defaultdict(set)
    edges_dense = dict()
    edges_scalar = dict()

    def insert_value(idx_row, idx_col, val):
        edges_scalar[idx_col, idx_row] = val

        node_idx_R = np.searchsorted(node_offsets, idx_row + 1) - 1
        node_idx_C = np.searchsorted(node_offsets, idx_col + 1) - 1
        node_R = node_names[node_idx_R]
        node_C = node_names[node_idx_C]
        offset_R = node_offsets_d[node_R]
        offset_C = node_offsets_d[node_C]
        edge = edges_dense.get((node_C, node_R), None)
        if edge is None:
            edge = np.zeros((node_sizes_d[node_R], node_sizes_d[node_C]))
            edges_dense[(node_C, node_R)] = edge
            seq[node_C].add(node_R)
            req[node_R].add(node_C)
        edge[idx_row - offset_R, idx_col - offset_C] = val

    def make_csc():
        data_ind = []
        row_ind = []
        col_ind = []
        for (idx_col, idx_row), val in edges_scalar.items():
            row_ind.append(idx_row)
            col_ind.append(idx_col)
            data_ind.append(val)
        return scisparse.csc_matrix(
            (data_ind, (row_ind, col_ind)), shape=(N, N), dtype=float,
        )

    def make_SREdense():
        return SRE_copy((seq, req, edges_dense))

    return locals()
