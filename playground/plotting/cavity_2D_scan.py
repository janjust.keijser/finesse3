import finesse
from finesse.analysis import x2axis

finesse.plotting.init()

#finesse.LOGGER.setLevel("INFO")

model = finesse.parse("""
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes even maxtem=4

gauss gL0 L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

pd refl ITM.p1.o
#pd circ ETM.p1.i
pd trns ETM.p2.o
""")
ifo = model.model

out = x2axis(ifo.ITM.phi, -180, 180, 300, ifo.ETM.phi, -180, 180, 300).run()

out.plot(figsize_scale=2, cmap={'refl' : 'cividis', 'trns' : 'viridis'}, log=True)
