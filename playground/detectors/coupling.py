import finesse
from finesse.analysis import noxaxis

finesse.LOGGER.setLevel("INFO")

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes maxtem=4

cav FP ITM.p2.o ITM.p2.i
"""
)
ifo.add(finesse.detectors.KnmDetector("k", ifo.ITM.p1.o, "11", 0, 2, 0, 0))

ifo.create_mismatch(ifo.ITM.p1.i, w0_mm=5, z_mm=2)

out = noxaxis(ifo)
print(out["k"])
