"""Example showing how to switch on logging within a context."""

from finesse.script import parse
from finesse.utilities import logs


script = """
laser l1 P=1
space s1 l1.p1 m1.p1
mirror m1 R=0.99 T=0.01
"""


print("Parsing without printed log messages...")
print(parse(script))
print()

with logs(
    level="info",
    fmt="[{levelname} / {name} / {asctime}] {message}",
    datefmt="%Y-%m-%d %H:%M",
):
    print("Parsing with printed log messages...")
    print(parse(script))
