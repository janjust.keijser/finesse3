import finesse
from finesse.components import Gauss

finesse.configure(plotting=True)

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1 L=1

m ITM R=0.9 T=0.1 Rc=-2.5
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=2.5

cav FP ITM.p2.o ITM.p2.i

bp cav_w w ITM.p2.o

mmd mmt ITM.p1 ITM.p2 percent=1

xaxis ETM.Rcx lin 2.5 10 200
#xaxis CAV.L lin 1 6 100
"""
)

model.ETM.Rcy = model.ETM.Rcx.ref
model.ITM.Rcx = -1 * model.ETM.Rcx.ref
model.ITM.Rcy = -1 * model.ETM.Rcx.ref

trace = model.beam_trace()
q_L0 = trace[model.L0.p1.o].qx

model.add(Gauss("gL0", model.L0.p1.o, q=q_L0))

out = model.run()
out.plot()
