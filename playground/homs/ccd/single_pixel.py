import finesse

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss gL0 L0.p1.o q=(-2+3j)
modes maxtem=2

ccdpx refl_px ITM.p1.o

xaxis ITM.xbeta lin 0 100u 400
"""
)

out = ifo.run()
out.plot()
