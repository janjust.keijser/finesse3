import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

CODE = """
l L0 1 0 n0
s s0 1 n0 nITM1

m ITM 0.9 0.1 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -2.5
attr ETM Rc 2.5

gauss* gL0 L0 n0 -1.535 1.3
cav FP ITM nITM2 ETM nETM1

xaxis CAV L lin 1 2 100

maxtem 4

pd circ nETM1
pd refl nITM1
pd trns nETM2
"""

model = finesse.Model()
model.parse_legacy(CODE)

# out = model.run()
# out.plot()
profile()

import pykat

base = pykat.finesse.kat(kat_code=CODE)
base.verbose = False

# out_p = base.run()
# out_p.plot()

profile("base.run()")

# import numpy as np

# assert np.allclose(out['circ'], out_p['circ'])
# assert np.allclose(out['refl'], out_p['refl'])
# assert np.allclose(out['trns'], out_p['trns'])
