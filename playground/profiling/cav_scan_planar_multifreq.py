import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 EOM.p1
mod EOM (
    f=100M
    midx=0.1
    order=6
)
s s1 EOM.p2 ITM.p1 L=1

m ITM R=0.99 T=0.01
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01

xaxis L0.f lin -100M 100M 500

pd circ ETM.p1.i
"""
)

profile()
