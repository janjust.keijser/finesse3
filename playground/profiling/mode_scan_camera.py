import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes maxtem=3

cav FP ITM.p2.o ITM.p2.i

ccd trns ETM.p2.o xlim=3 ylim=3 npts=300

xaxis ITM.phi lin -180 0 150
"""
)

model.L0.tem(0, 2, 0.8)
model.L0.tem(1, 2, 0.4)
model.L0.tem(3, 0, 0.5)
model.L0.tem(1, 1, 0.3)

profile()
