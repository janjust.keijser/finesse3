import finesse

michelson = finesse.parse(
    """
l L0 P=1
s s0 L0.p1 BS.p1 L=1

bs BS R=0.5 T=0.5

s sN BS.p2 ITMY.p1 L=0.5

m ITMY R=0.99 T=0.01 Rc=(-0.5)
s sY ITMY.p2 ETMY.p1 L=1
m ETMY R=0.99 T=0.01 Rc=0.5

s sE BS.p3 ITMX.p1 L=0.4

m ITMX R=0.99 T=0.01 Rc=(-2.6)
s sX ITMX.p2 ETMX.p2 L=1
m ETMX R=0.99 T=0.01 Rc=2.7

modes maxtem=4

pd out BS.p4.o

cav FPX ITMX.p2.o ITMX.p2.i
cav FPY ITMY.p2.o ITMY.p2.i
"""
)
ifo = michelson.model

print(ifo.beam_trace())
