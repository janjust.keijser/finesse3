"""
Not really a concrete test but a bunch of random symbol stuff from a notebook that should all run without exceptions
"""
import finesse

kat = finesse.Model()

kat.parse_legacy(
    """
l l1 1 0 n0
s s1 0 n0 n1
mod eo1 10000 .05 5 pm n1 n2
ad bessel0 0 n2
ad bessel1 2 n2
ad bessel2 2 n2
ad bessel3 3 n2
"""
)


kat.add(finesse.components.Variable("fmod", 100))

(kat.eo1.f.ref + kat.l1.f.ref) == (kat.l1.f.ref + kat.eo1.f.ref)

kat.eo1.f.ref * 3

kat.eo1.f = kat.fmod.value.ref
kat.eo1.f

a = (kat.fmod.value.ref * 2) ** 2 + 4

kat.eo1.f = kat.fmod.value
kat.eo1.f

import math

cos = lambda x: finesse.element.Operation("cos", math.cos, x)
sin = lambda x: finesse.element.Operation("sin", math.sin, x)
atan2 = lambda y, x: finesse.element.Operation("atan2", math.atan2, y, x)

sin(cos(kat.fmod.value.ref * 2)) * atan2(kat.fmod.value.ref, 2)

y = sin(cos(kat.fmod.value.ref * 2)) * atan2(kat.fmod.value.ref, 2)
kat.fmod.f = 45000

import numpy as np

angle = lambda x: finesse.element.Operation("angle", np.angle, x)
f"{angle(1+1j)} = {angle(1+1j).eval()}"

y = kat.eo1.f.ref + kat.l1.f.ref
y.eval(subs={kat.fmod.value: np.linspace(0, 100, 5)})
