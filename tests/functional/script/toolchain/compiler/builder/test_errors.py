import pytest
from finesse.script.exceptions import KatScriptError
from finesse.symbols import OPERATORS, FUNCTIONS
from ......util import escape_full


@pytest.fixture
def compiler(compiler, fake_element_cls, fake_binop_cls, fake_unop_cls):
    class fake_element_with_args(fake_element_cls):
        """A fake element that takes fixed arguments."""

        def __init__(self, name, a, b):
            pass

    fake_element_with_kwargs = fake_element_cls

    class fake_element_with_args_kwargs(fake_element_cls):
        """A fake element that takes positional and keyword arguments."""

        def __init__(self, name, a, b, c, d=None):
            pass

    # Define an element setter proxy with positional arg requirements so we can test
    # errors.
    def set_element_setterproxy(name, a, b):
        pass

    # Define a command setter proxy with positional arg requirements so we can test
    # errors.
    def set_command_with_args(model, a, b, c):
        pass

    # Define a command setter proxy with keyword arg requirements so we can test errors.
    def set_command_with_kwargs(model, a=None, b=None, c=None):
        pass

    # Define a command setter with positional and keyword arg requirements so we can test
    # errors.
    def set_command_with_args_kwargs(model, a, b, c, d=None):
        pass

    compiler.spec.register_element(
        "fake_element_with_args",
        {"setter": fake_element_with_args, "getter": fake_element_with_args},
    )
    compiler.spec.register_element(
        "fake_element_with_kwargs",
        {"setter": fake_element_with_kwargs, "getter": fake_element_with_kwargs},
    )
    compiler.spec.register_element(
        "fake_element_mixed",
        {
            "setter": fake_element_with_args_kwargs,
            "getter": fake_element_with_args_kwargs,
        },
    )
    compiler.spec.register_element(
        "fake_element_setterproxy",
        {"setter": set_element_setterproxy, "getter": lambda: None},
    )
    compiler.spec.register_command(
        "fake_command_with_args",
        {"setter": set_command_with_args, "getter": lambda: None, "singular": True,},
    )
    compiler.spec.register_command(
        "fake_command_with_kwargs",
        {"setter": set_command_with_kwargs, "getter": lambda: None, "singular": True,},
    )
    compiler.spec.register_command(
        "fake_command_mixed",
        {
            "setter": set_command_with_args_kwargs,
            "getter": lambda: None,
            "singular": True,
        },
    )
    # Have to use real Finesse operators here because the builder matches against Finesse
    # operations.
    compiler.spec.binary_operators["+"] = OPERATORS["__add__"]
    compiler.spec.unary_operators["-"] = FUNCTIONS["neg"]

    return compiler


@pytest.mark.parametrize(
    "script,error",
    (
        ## Elements
        # The error handler should compensate for the `self` argument of the setters
        # that are not exposed in KatScript. We also consider the `name` parameter not
        # to be an "argument" in KatScript, so that gets dealt with too.
        # Element with positional arguments.
        (
            "fake_element_with_args myelement",
            "\nline 1: 'fake_element_with_args' missing 2 required positional arguments: 'a' and 'b'\n"
            "-->1: fake_element_with_args myelement\n"
            "                                       ^\n"
            "Syntax: fake_element_with_args name a b",
        ),
        (
            "fake_element_with_args myelement 1",
            "\nline 1: 'fake_element_with_args' missing 1 required positional argument: 'b'\n"
            "-->1: fake_element_with_args myelement 1\n"
            "                                         ^\n"
            "Syntax: fake_element_with_args name a b",
        ),
        # Element with positional and keyword arguments - no arguments.
        (
            "fake_element_mixed myelement",
            "\nline 1: 'fake_element_mixed' missing 3 required positional arguments: 'a', 'b', and 'c'\n"
            "-->1: fake_element_mixed myelement\n"
            "                                   ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with positional and keyword arguments - called by position.
        (
            "fake_element_mixed myelement 1",
            "\nline 1: 'fake_element_mixed' missing 2 required positional arguments: 'b' and 'c'\n"
            "-->1: fake_element_mixed myelement 1\n"
            "                                     ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with positional and keyword arguments - called by keyword.
        (
            "fake_element_mixed myelement a=1",
            "\nline 1: 'fake_element_mixed' missing 2 required positional arguments: 'b' and 'c'\n"
            "-->1: fake_element_mixed myelement a=1\n"
            "                                       ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with positional and keyword arguments - mixed call.
        (
            "fake_element_mixed myelement 1 b=2",
            "\nline 1: 'fake_element_mixed' missing 1 required positional argument: 'c'\n"
            "-->1: fake_element_mixed myelement 1 b=2\n"
            "                                         ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with a setter proxy. There is no `self`, but there is still a `name`.
        # The error handler has to subtract only 1 from the numbers in the Python
        # exception.
        (
            "fake_element_setterproxy myelement",
            "\nline 1: 'fake_element_setterproxy' missing 2 required positional arguments: 'a' and 'b'\n"
            "-->1: fake_element_setterproxy myelement\n"
            "                                         ^\n"
            "Syntax: fake_element_setterproxy a b",
        ),
        (
            "fake_element_setterproxy myelement 1",
            "\nline 1: 'fake_element_setterproxy' missing 1 required positional argument: 'b'\n"
            "-->1: fake_element_setterproxy myelement 1\n"
            "                                           ^\n"
            "Syntax: fake_element_setterproxy a b",
        ),
        ## Commands
        # The error handler should compensate for the `self` (for direct setters) or
        # `model` (for setter proxies) of the setter arguments that are not exposed in
        # KatScript.
        # Command with positional arguments.
        (
            "fake_command_with_args()",
            "\nline 1: 'fake_command_with_args' missing 3 required positional arguments: 'a', 'b', and 'c'\n"
            "-->1: fake_command_with_args()\n"
            "                             ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1)",
            "\nline 1: 'fake_command_with_args' missing 2 required positional arguments: 'b' and 'c'\n"
            "-->1: fake_command_with_args(1)\n"
            "                              ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1, 2)",
            "\nline 1: 'fake_command_with_args' missing 1 required positional argument: 'c'\n"
            "-->1: fake_command_with_args(1, 2)\n"
            "                                 ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        # Command with positional and keyword arguments - no arguments.
        (
            "fake_command_mixed()",
            "\nline 1: 'fake_command_mixed' missing 3 required positional arguments: 'a', 'b', and 'c'\n"
            "-->1: fake_command_mixed()\n"
            "                         ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        # Command with positional and keyword arguments - called by position.
        (
            "fake_command_mixed(1)",
            "\nline 1: 'fake_command_mixed' missing 2 required positional arguments: 'b' and 'c'\n"
            "-->1: fake_command_mixed(1)\n"
            "                          ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        (
            "fake_command_mixed(1, 2)",
            "\nline 1: 'fake_command_mixed' missing 1 required positional argument: 'c'\n"
            "-->1: fake_command_mixed(1, 2)\n"
            "                             ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        # Command with positional and keyword arguments - called by keyword.
        (
            "fake_command_mixed(a=1)",
            "\nline 1: 'fake_command_mixed' missing 2 required positional arguments: 'b' and 'c'\n"
            "-->1: fake_command_mixed(a=1)\n"
            "                            ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        (
            "fake_command_mixed(a=1, b=2)",
            "\nline 1: 'fake_command_mixed' missing 1 required positional argument: 'c'\n"
            "-->1: fake_command_mixed(a=1, b=2)\n"
            "                                 ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        # Command with positional and keyword arguments - mixed call.
        (
            "fake_command_mixed(1, b=2)",
            "\nline 1: 'fake_command_mixed' missing 1 required positional argument: 'c'\n"
            "-->1: fake_command_mixed(1, b=2)\n"
            "                               ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
    ),
)
def test_not_enough_arguments(compiler, script, error):
    with pytest.raises(KatScriptError, match=escape_full(error)):
        compiler.compile(script)


@pytest.mark.parametrize(
    "script,error",
    (
        ## Elements
        # Element with positional arguments.
        (
            "fake_element_with_args myelement 1 2 3",
            "\nline 1: 'fake_element_with_args' takes 2 positional arguments but 3 were given\n"
            "-->1: fake_element_with_args myelement 1 2 3\n"
            "                                           ^\n"
            "Syntax: fake_element_with_args name a b",
        ),
        (
            "fake_element_with_args myelement 1 2 3 4",
            "\nline 1: 'fake_element_with_args' takes 2 positional arguments but 4 were given\n"
            "-->1: fake_element_with_args myelement 1 2 3 4\n"
            "                                           ^ ^\n"
            "Syntax: fake_element_with_args name a b",
        ),
        # Element with keyword arguments.
        (
            "fake_element_with_kwargs myelement 1 2 3",
            "\nline 1: 'fake_element_with_kwargs' takes from 0 to 2 positional arguments but 3 were given\n"
            "-->1: fake_element_with_kwargs myelement 1 2 3\n"
            "                                             ^\n"
            "Syntax: fake_element_with_kwargs name a=none b=none",
        ),
        (
            "fake_element_with_kwargs myelement 1 2 3 4",
            "\nline 1: 'fake_element_with_kwargs' takes from 0 to 2 positional arguments but 4 were given\n"
            "-->1: fake_element_with_kwargs myelement 1 2 3 4\n"
            "                                             ^ ^\n"
            "Syntax: fake_element_with_kwargs name a=none b=none",
        ),
        # Element with positional and keyword arguments - called by position.
        (
            "fake_element_mixed myelement 1 2 3 4 5",
            "\nline 1: 'fake_element_mixed' takes from 3 to 4 positional arguments but 5 were given\n"
            "-->1: fake_element_mixed myelement 1 2 3 4 5\n"
            "                                           ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        (
            "fake_element_mixed myelement 1 2 3 4 5 6",
            "\nline 1: 'fake_element_mixed' takes from 3 to 4 positional arguments but 6 were given\n"
            "-->1: fake_element_mixed myelement 1 2 3 4 5 6\n"
            "                                           ^ ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with positional and keyword arguments - called by keyword.
        (
            "fake_element_mixed myelement a=1 b=2 c=3 d=4 e=5",
            "\nline 1: 'fake_element_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_element_mixed myelement a=1 b=2 c=3 d=4 e=5\n"
            "                                                   ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        (
            "fake_element_mixed myelement a=1 b=2 c=3 d=4 e=5 f=6",
            "\nline 1: 'fake_element_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_element_mixed myelement a=1 b=2 c=3 d=4 e=5 f=6\n"
            "                                                   ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with positional and keyword arguments - mixed call.
        (
            "fake_element_mixed myelement 1 2 3 d=4 e=5",
            "\nline 1: 'fake_element_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_element_mixed myelement 1 2 3 d=4 e=5\n"
            "                                             ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        (
            "fake_element_mixed myelement 1 2 3 d=4 e=5 f=6",
            "\nline 1: 'fake_element_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_element_mixed myelement 1 2 3 d=4 e=5 f=6\n"
            "                                             ^\n"
            "Syntax: fake_element_mixed name a b c d=none",
        ),
        # Element with a setter proxy.
        (
            "fake_element_setterproxy myelement 1 2 3",
            "\nline 1: 'fake_element_setterproxy' takes 2 positional arguments but 3 were given\n"
            "-->1: fake_element_setterproxy myelement 1 2 3\n"
            "                                             ^\n"
            "Syntax: fake_element_setterproxy a b",
        ),
        (
            "fake_element_setterproxy myelement 1 2 3 4",
            "\nline 1: 'fake_element_setterproxy' takes 2 positional arguments but 4 were given\n"
            "-->1: fake_element_setterproxy myelement 1 2 3 4\n"
            "                                             ^ ^\n"
            "Syntax: fake_element_setterproxy a b",
        ),
        ## Commands
        # Command with positional arguments.
        (
            "fake_command_with_args(1, 2, 3, 4)",
            "\nline 1: 'fake_command_with_args' takes 3 positional arguments but 4 were given\n"
            "-->1: fake_command_with_args(1, 2, 3, 4)\n"
            "                                      ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1, 2, 3, 4, 5)",
            "\nline 1: 'fake_command_with_args' takes 3 positional arguments but 5 were given\n"
            "-->1: fake_command_with_args(1, 2, 3, 4, 5)\n"
            "                                      ^  ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        # Command with keyword arguments.
        (
            "fake_command_with_kwargs(1, 2, 3, 4)",
            "\nline 1: 'fake_command_with_kwargs' takes from 0 to 3 positional arguments but 4 were given\n"
            "-->1: fake_command_with_kwargs(1, 2, 3, 4)\n"
            "                                        ^\n"
            "Syntax: fake_command_with_kwargs(a=none, b=none, c=none)",
        ),
        (
            "fake_command_with_kwargs(1, 2, 3, 4, 5)",
            "\nline 1: 'fake_command_with_kwargs' takes from 0 to 3 positional arguments but 5 were given\n"
            "-->1: fake_command_with_kwargs(1, 2, 3, 4, 5)\n"
            "                                        ^  ^\n"
            "Syntax: fake_command_with_kwargs(a=none, b=none, c=none)",
        ),
        # Command with positional and keyword arguments - called by position.
        (
            "fake_command_mixed(1, 2, 3, 4, 5)",
            "\nline 1: 'fake_command_mixed' takes from 3 to 4 positional arguments but 5 were given\n"
            "-->1: fake_command_mixed(1, 2, 3, 4, 5)\n"
            "                                     ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        (
            "fake_command_mixed(1, 2, 3, 4, 5, 6)",
            "\nline 1: 'fake_command_mixed' takes from 3 to 4 positional arguments but 6 were given\n"
            "-->1: fake_command_mixed(1, 2, 3, 4, 5, 6)\n"
            "                                     ^  ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        # Command with positional and keyword arguments - called by keyword.
        (
            "fake_command_mixed(a=1, b=2, c=3, d=4, e=5)",
            "\nline 1: 'fake_command_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_command_mixed(a=1, b=2, c=3, d=4, e=5)\n"
            "                                             ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
        (
            "fake_command_mixed(a=1, b=2, c=3, d=4, e=5, f=6)",
            "\nline 1: 'fake_command_mixed' got an unexpected keyword argument 'e'\n"
            "-->1: fake_command_mixed(a=1, b=2, c=3, d=4, e=5, f=6)\n"
            "                                             ^\n"
            "Syntax: fake_command_mixed(a, b, c, d=none)",
        ),
    ),
)
def test_too_many_arguments(compiler, script, error):
    with pytest.raises(KatScriptError, match=escape_full(error)):
        compiler.compile(script)


@pytest.mark.parametrize(
    "script,error",
    (
        (
            "fake_command_with_args(1, 2, 3, a=1)",
            "\nline 1: 'fake_command_with_args' got multiple values for argument 'a'\n"
            "-->1: fake_command_with_args(1, 2, 3, a=1)\n"
            "                             ^          ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1, 2, 3, 4, a=1)",
            "\nline 1: 'fake_command_with_args' got multiple values for argument 'a'\n"
            "-->1: fake_command_with_args(1, 2, 3, 4, a=1)\n"
            "                             ^             ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1, 2, 3, 4, 5, a=1)",
            "\nline 1: 'fake_command_with_args' got multiple values for argument 'a'\n"
            "-->1: fake_command_with_args(1, 2, 3, 4, 5, a=1)\n"
            "                             ^                ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
        (
            "fake_command_with_args(1, 2, 3, a=1, b=2)",
            "\nline 1: 'fake_command_with_args' got multiple values for argument 'a'\n"
            "-->1: fake_command_with_args(1, 2, 3, a=1, b=2)\n"
            "                             ^          ^\n"
            "Syntax: fake_command_with_args(a, b, c)",
        ),
    ),
)
def test_duplicate_arguments(compiler, script, error):
    """Duplicate arguments are not allowed.

    Note: duplicate keyword arguments (i.e. multiple arguments specified with the same
    keyword) are tested in ../resolver/test_arguments.py as they are caught by the
    resolver, not builder.
    """
    with pytest.raises(KatScriptError, match=escape_full(error)):
        compiler.compile(script)
