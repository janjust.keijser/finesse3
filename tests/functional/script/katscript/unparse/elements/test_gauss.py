import pytest
from finesse.components import Mirror
from finesse.components.gauss import Gauss
from finesse.script import unparse
from finesse.script.generator import ElementContainer


@pytest.mark.parametrize("node", ("m1.p1.i", "m1.p1.o", "m1.p2.i", "m1.p2.o"))
@pytest.mark.parametrize(
    "beam_params",
    (
        {"w0": 10e-3, "z": -1200},
        {"z": 100e-3, "zr": 100},
        {"w": 1e-3, "rc": -1},
        {"w": 1e-3, "rc": float("inf")},
        {"w": 3.141e-6, "S": 0},
        {"w": 3.141e-6, "S": 1e6},
        {"q": 1 + 1j},
        {"q": -0.1 + 2.3j},
    ),
)
def test_gauss(model, node, beam_params):
    model.add(Mirror("m1"))
    model.add(Gauss("g1", model.reduce_get_attr(node), **beam_params))

    # The gauss needs wrapped in a :class:`.ElementContainer` to make it unparse.
    script = unparse(ElementContainer(model.g1))
    expected_params = " ".join(f"{k}={unparse(v)}" for k, v in beam_params.items())
    expected = f"gauss g1 node={node} {expected_params}"
    assert script == expected
