import pytest
from finesse.components import Mirror
from finesse.analysis.actions import Xaxis
from finesse.script import unparse


model_parameters = pytest.mark.parametrize(
    "parameter",
    ("m1.R", "m1.T", "m1.L", "m1.phi", "m1.Rcx", "m1.Rcy", "m1.xbeta", "m1.ybeta"),
)
axis_scales = pytest.mark.parametrize("scale", ("lin", "log"))
axis_sweeps = pytest.mark.parametrize(
    "start,stop,steps",
    ((1, 90, 1000),),  # Not zero to avoid division by zero when used with log scale.
)
relative = pytest.mark.parametrize("relative", (True, False))


@pytest.fixture
def sweep_model(model):
    model.add(Mirror("m1"))
    return model


@model_parameters
@axis_scales
@axis_sweeps
@relative
def test_xaxis(sweep_model, parameter, scale, start, stop, steps, relative):
    sweep_model.analysis = Xaxis(
        sweep_model.reduce_get_attr(parameter),
        scale,
        start,
        stop,
        steps,
        relative=relative,
    )
    script = unparse(sweep_model.analysis)
    expected = (
        f"xaxis("
        f"parameter={parameter}, mode={scale}, start={start}, stop={stop}, "
        f"steps={steps}, relative={unparse(relative)}, pre_step=none, post_step=none, "
        f"name='xaxis'"
        f")"
    )
    assert script == expected
